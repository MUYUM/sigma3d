# Sigma3D #
A library for displaying measurements over top of pointclouds and IFC models.

# How to use: #
- npm install
- npx webpack --config webpack.config.js
- Check tests/ and run any .html file on a local server

# Todo #
	- Minor fixes:
		- Fix measurement scaling
		- Make camera snap to most recently added object
		- add Models array to Image360Source and create a general Source class to inherit from
		- add warnings for when the user does something wrong
		- expand event system ("layer-added", etc.)

	- "Snap" interactions
	- "Draw" interactions:
		- See if its necessary to calculate an average Z when the scene is empty

	- Pointcloud styling

	- Make the view control:
		- Camera type
