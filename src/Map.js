"use strict";

import * as THREE from '../libs/threejs/three.module.js';

import { Utilities } from './utilities/Utilities.js';

class Map {
    constructor( config ) {
        this.Layers = [];
        this.Interactions = [];

        this.Light = new THREE.AmbientLight( 'white' );
        this.PotreeViewer = Utilities.PotreeViewer;

        if (config) {
            if (config.view) this.CurrentView = config.view;
            if (config.layers) this.initializeLayers(config.layers);
            if (config.interactions) this.initializeInteractions(config.interactions);
        }

        this.everyFrame();
    }

    async initializeLayers(layers) {
        for (let layer of layers) {
            await this.addLayer(layer)
        }

        this.PotreeViewer.scene.scene.add( this.Light );
    }

    async initializeInteractions(interactions) {
        for (let interaction of interactions) {
            await this.addInteraction(interaction)
        }
    }

    async addLayer( layer ) {
        await layer.Source.load()
        layer.Source.attachToScene();

        if (layer.Source.Models && layer.Source.Models[0]) {
            this.CurrentView.zoomToObject(layer.Source.Models[0], 2, 0);
        }

        this.Layers.push(layer);
    }

    async addInteraction( interaction ) {
        await interaction.initializeEvents();
        this.Interactions.push(interaction);
    }

    everyFrame() {
        requestAnimationFrame(this.everyFrame.bind(this));
    }

}

export { Map }