"use strict";

import * as THREE from '../libs/threejs/three.module.js';

import {Map} from './Map.js';
import {View} from './View.js';

import { Utilities } from './utilities/Utilities.js'
import * as Geometries from './utilities/Geometries.js'
import * as Materials from './utilities/Materials.js'

// Never import the default Layer class, it's meant only for inheritance
import {PointcloudLayer} from './layers/PointcloudLayer.js';
import {IFCLayer} from './layers/IFCLayer.js';
import {GeometryLayer} from './layers/GeometryLayer.js';
import {Image360Layer} from './layers/Image360Layer.js';

// Never import the default Interactions class, it's meant only for inheritance
import { SelectInteraction } from './interactions/SelectInteraction.js';
import { DrawInteraction } from './interactions/DrawInteraction.js';

import { GeometrySource } from './sources/GeometrySource.js';
import { IFCSource } from './sources/IFCSource.js';
import { PointcloudSource } from './sources/PointcloudSource.js';
import { Image360Source } from './sources/Image360Source.js';

import { Point } from './geometries/Point.js'
import { HeightPoint } from './geometries/HeightPoint.js'
import { Line } from './geometries/Line.js'
import { Polygon } from './geometries/Polygon.js'

class Sigma3D {
    
    constructor() {
        // This is the current "Map". It defines layers and interactions.
        this.CurrentMap = undefined;

        // Every class of the library
        this.View = View;
        this.Map = Map;

            // Layers
        this.PointcloudLayer = PointcloudLayer;
        this.IFCLayer = IFCLayer;
        this.GeometryLayer = GeometryLayer;
        this.Image360Layer = Image360Layer;

            // interactions
        this.SelectInteraction = SelectInteraction;
        this.DrawInteraction = DrawInteraction;

            // sources
        this.GeometrySource = GeometrySource;
        this.IFCSource = IFCSource;
        this.PointcloudSource = PointcloudSource;
        this.Image360Source = Image360Source;

            // utilities
        this.Utilities = Utilities;
        this.Geometries = Geometries;
        this.Materials = Materials;

            // geometries
        this.Point = Point;
        this.HeightPoint = HeightPoint;
        this.Line = Line;
        this.Polygon = Polygon;

            // controls
        this.DeviceOrientationControls = Potree.DeviceOrientationControls;
        this.EarthControls = Potree.EarthControls;
        this.VRControls = Potree.VRControls;
        this.OrbitControls = Potree.OrbitControls;
        this.noCloudControls = Utilities.noCloudControls;

    }

    setMap( newMap ) {
        this.CurrentMap = newMap;
    }
}

window.Sigma3D = new Sigma3D();