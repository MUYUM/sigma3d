"use strict"

import * as THREE from '../libs/threejs/three.module.js';
import {Utilities} from '../src/utilities/Utilities.js'

class View {
    constructor( config ) {
        this.Position = config.position;
        this.PotreeViewer = Utilities.PotreeViewer;

        if (config.controls) {
            this.PotreeViewer.setControls( config.controls );
        }
    }

    // Used by the user to change what the view is looking at.
    zoomToObject(object) {
        this.PotreeViewer.zoomTo(object, 1, 0);

        if (this.PotreeViewer.controls.pivot) {
            this.PotreeViewer.controls.pivot.set(object.position);
        }

        // Adjust the target Z of nocloudcontrols
        if (this.PotreeViewer.controls.targetZ != undefined || this.PotreeViewer.controls.targetZ != null) {
            this.PotreeViewer.controls.targetZ = object.position.z;
        }

        this.PotreeViewer.controls.stop();
    }

    zoomToPosition( [x,y,z] ) {
        let pivot = new THREE.AxesHelper(6);
        pivot.position.set(x,y,z);

        this.PotreeViewer.zoomTo(pivot, 1, 0);
        
        if (this.PotreeViewer.controls.pivot) {
            this.PotreeViewer.controls.pivot.set(pivot.position);
        }

        if (this.PotreeViewer.controls.targetZ) {
            this.PotreeViewer.controls.targetZ = pivot.position.z;
        }

        this.PotreeViewer.controls.stop();
    }


}

export { View }