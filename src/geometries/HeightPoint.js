'use strict';

import { Geometry } from './Geometry.js';

import * as THREE from '../../libs/threejs/three.module.js';

import {HeightPointGeometry, TextGeometry} from '../utilities/Geometries.js'
import {HeightPointMaterial} from '../utilities/Materials.js'

class HeightPoint extends Geometry {
    constructor( [x, y, z], config ) { // Things like label offset will be inside the config object in the future.      
        super();
        this.Vectors = [ [x,y,z] ];

        // Point model
        let material = HeightPointMaterial;

        if (config && config.material) {
            material = config.material;
        }

        this.model = new THREE.Mesh( HeightPointGeometry, material );
        this.model.position.set(x, y, z);
        
        // Height label model
        this.labelModel = this.generateHeightLabelModel(x, y, z);
        this.labelModel.position.set( (x + 0.5), (y + 0.5), z );

        this.model.attach(this.labelModel);

        // Reference to its own class data
        this.Type = "HeightPoint"
        this.model.userData = this;
    }

    generateHeightLabelModel( height ) {
        let heightText = Math.round(height * 100) / 100;
        if ( Number.isInteger(heightText)) heightText = heightText + ".00";
        
        let texture = new THREE.Texture( this.createTextCanvas(heightText, {fontSize:32, color:'black'}) );
        texture.needsUpdate = true;

        let material = new THREE.MeshBasicMaterial({map: texture, transparent: true, opacity: 0.9, color: 'white', side:THREE.DoubleSide});

        let labelModel = new THREE.Mesh( TextGeometry, material);

        return labelModel;
    }
    
    createTextCanvas(string, parameters = {}){
        let canvas = document.createElement("canvas");
        let ctx = canvas.getContext("2d");

        // Prepare the font to be able to measure
        let fontSize = parameters.fontSize || 56;
        ctx.font = `${fontSize}px sans-serif`;

        const textMetrics = ctx.measureText(string);

        let width = textMetrics.width;
        let height = fontSize;

        // Resize canvas to match text size 
        canvas.width = width;
        canvas.height = height;
        canvas.style.width = width + "px";
        canvas.style.height = height + "px";

        // Re-apply font since canvas is resized.
        ctx.font = `${fontSize}px sans-serif`;
        ctx.textAlign = parameters.align || "center" ;
        ctx.textBaseline = parameters.baseline || "middle";

        // Make the canvas transparent for simplicity
        ctx.fillStyle = "transparent";
        ctx.fillRect(0, 0, ctx.canvas.width, ctx.canvas.height);

        ctx.fillStyle = parameters.color || "white";
        ctx.fillText(string, width / 2, height / 2);

        canvas.remove();

        return canvas;
    }

    updateVectors() {
        this.Vectors = [ this.model.position.x, this.model.position.y, this.model.position.z ];
    }

}

export{HeightPoint};