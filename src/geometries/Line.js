'use strict';
import { Geometry } from './Geometry.js';

import * as THREE from '../../libs/threejs/three.module.js';

import {LineMaterial} from '../utilities/Materials.js'

class Line extends Geometry {
    
    constructor( positions, config ) {    
        super();
        
        this.Vectors = positions;

        let threeVectors = this.convertPositionsToVectors( positions ); // turns [x,y,z] into a THREE.Vector3() instance
        let geometry = new THREE.BufferGeometry().setFromPoints( threeVectors ); // Every line has a unique geometry, meaning it cannot be stored in the Geometries.js file.

        // Properties
        let material = LineMaterial;

        if (config && config.material) {
            material = config.material;
        }

        this.model = new THREE.Line( geometry, material );

        // Reference to its own class data, this is useful when its model is clicked in three.js and it needs access to it's own data
        this.Type = "Line"
        this.model.userData = this;
    }

    convertPositionsToVectors( positions ) {
        let vectors = [];

        for (let [x,y,z] of positions) {
            vectors.push( new THREE.Vector3(x,y,z) );
        }
        
        return vectors;
    }

}

export { Line };

