'use strict';

import { Geometry } from './Geometry.js';

import * as THREE from '../../libs/threejs/three.module.js';

import {PointGeometry} from '../utilities/Geometries.js'
import {PointMaterial} from '../utilities/Materials.js'

class Point extends Geometry {
    
    constructor( [x, y, z], config ) {    
        super();
        this.Vectors = [ [x,y,z] ] 

        // Properties      
        let material = PointMaterial

        if (config && config.material) {
            material = config.material;
        }

        this.model = new THREE.Mesh( PointGeometry, material );
        this.model.position.set(x, y, z)

        // Reference to its own class data, this is useful when its model is clicked in three.js and it needs access to it's own data
        this.Type = "Point";
        this.model.userData = this;
    }

    updateVectors() {
        this.Vectors = [ this.model.position.x, this.model.position.y, this.model.position.z ];
    }

}

export { Point };

