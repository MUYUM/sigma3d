"use strict";

import { Geometry } from './Geometry.js';

import * as THREE from '../../libs/threejs/three.module.js';

import {earcut} from '../../libs/earcut/earcut.js' // Used for triangulation.
import '../../libs/turf/turf.min.js' // Used for polygon centroids -- Import might be broken, we will see. --

import {PolygonMaterial} from '../utilities/Materials.js'


class Polygon extends Geometry {

    // Positions is an array of coordinates. It is structured according to how polygons work in geoJSON.
    // The first set of coordinates is used to represent the polygon, while each subsequent array represents holes within the polygon.
    // [ [ [0,0], [3,3], [5,5] ], [ [1,1] [2,2], [3,3] ] ]

    constructor(positions, config) {
        super();
        
        this.Vectors = this.getVectors(positions);

        let material = PolygonMaterial;

        if (config && config.material) {
            material = config.material;
        }
        
        this.model = this.generateModel( positions, material );

        // Polygons can have a centroid object.
        this.centroid = undefined

        // Reference to its own class data, this is useful when its model is clicked in three.js and it needs access to it's own data
        this.Type = "Polygon";
        this.model.userData = this;
    }

    getVectors(positions) {
        let vectors = [];
        let earcutVertices = earcut.flatten(positions).vertices;

        for (let i=0; i < earcutVertices.length; i+=3) {
            vectors.push( [ earcutVertices[i], earcutVertices[i+1], earcutVertices[i+2] ]);
        }

        return vectors;
    }

    generateModel( positions, material ) {
        let data = earcut.flatten(positions);
        let triangles = earcut(data.vertices, data.holes, data.dimensions);

        let geometry = new THREE.Geometry();

        for (let i=0; i < data.vertices.length; i += 3) {
            geometry.vertices.push( new THREE.Vector3(data.vertices[i], data.vertices[i + 1], data.vertices[i +2]) )
        }

        for (let i=0; i < triangles.length; i+=3) {
            geometry.faces.push( new THREE.Face3(triangles[i], triangles[i+1], triangles[i+2]) )
        }

        geometry.computeFaceNormals();
        
        let model = new THREE.Mesh(geometry, material);
        return model;
    }

    generateCentroid() {
        /*
            var polygon = turf.polygon([[
                [116, -36],
                [131, -32],
                [146, -43],
                [155, -25],
                [133, -9],
                [111, -22],
                [116, -36]
            ]]);

            var pointOnPolygon = turf.pointOnFeature(polygon);
        */
    }

    addCentroid( centroid ) {
        this.centroid = centroid;
    }

}

export {Polygon}
