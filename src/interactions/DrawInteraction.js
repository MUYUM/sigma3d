import { Interaction } from './Interaction.js';
import { Utilities } from '../utilities/Utilities.js';

import { DrawHelper } from './helpers/DrawHelper.js';

import * as THREE from '../../libs/threejs/three.module.js';

export class DrawInteraction extends Interaction {
    
    constructor(config) {
        super(config);

        if (config) {
            if (config.source) this.ParentSource = config.source;
            if (config.type) {
                this.Type = config.type;

                this.DrawHelper = new DrawHelper({
                    type: config.type
                });
            }
        }

        this.MovedMouse = false;
        this.Vectors = []; // [ [x,y,z], [x,y,z]... ] 

    }

    async initializeEvents() {
        this.DomElement.addEventListener('pointerdown', (event) => { 
            this.MovedMouse = false;
        });

        this.DomElement.addEventListener('mousemove', (event) => {
            this.MovedMouse = true;
        });

        this.DomElement.addEventListener('pointerup', async (event) => {
            if (event.button == 0) { // left click
                if (this.MovedMouse == false) {
                    let position = this.getMousePosition(event);
                    this.Vectors.push(position);

                    if (this.DrawHelper) {
                        this.DrawHelper.addVector( [position.x, position.y, position.z] );
                    }
                }
            }

            else if (event.button == 2) { // right click
                this.Vectors = [];

                if (this.DrawHelper) {
                    let drawnGeometry = [];

                    if (this.Type == "Point" || this.Type == "HeightPoint") {
                        drawnGeometry = this.DrawHelper.Points;
                    }
                    else if (this.Type == "Line") {
                        drawnGeometry = [ this.DrawHelper.Line ];
                    }
                    else if (this.Type == "Polygon") {
                        drawnGeometry = [ this.DrawHelper.Polygon ];
                    }

                    const customEvent = new CustomEvent('drawend', { detail: drawnGeometry });
                    this.EventDispatcher.dispatchEvent(customEvent);

                    this.DrawHelper.clear();
                }
            }
        });
    }

    getMousePosition(event) {
        let vector = new THREE.Vector3(); // create once and reuse
        let mouse = new THREE.Vector2(
            ( event.layerX / this.DomElement.getBoundingClientRect().width) * 2 - 1, // x
            -( event.layerY / this.DomElement.getBoundingClientRect().height ) * 2 + 1 // y
        );

        vector.set(mouse.x, mouse.y, 0.5);
        vector.unproject( this.PotreeViewer.scene.getActiveCamera().clone() );
        vector.sub( this.PotreeViewer.scene.getActiveCamera().clone().position ).normalize();
        
        let intersectZ = this.getIntersectZ(mouse); 
        
        let distance = (intersectZ - this.PotreeViewer.scene.getActiveCamera().clone().position.z) / vector.z;
        let position = (this.PotreeViewer.scene.getActiveCamera().clone().position).add( vector.multiplyScalar( distance ) );
        
        return position;
    }

    getIntersectZ(mouse) {
        let intersectZ = 0;

        this.RayCaster.setFromCamera( mouse, this.PotreeViewer.scene.getActiveCamera() );
        let intersected = this.RayCaster.intersectObjects(  this.PotreeViewer.scene.scene.children, false );

        if ( intersected.length ) {
            intersectZ = intersected[0].point.z;
        }

        return intersectZ;
    }

}