import {Utilities} from '../utilities/Utilities.js'
import * as THREE from '../../libs/threejs/three.module.js';

/* 
    Interactions bind different behaviors to a map based on the parent layer they are assigned to.
    They assign their events to the DomElement of the PotreeViewer renderer.
*/

export class Interaction {
    
    constructor() {
        this.ParentSource = undefined; // This is assigned by the layer when adding the interaction
        this.PotreeViewer = Utilities.PotreeViewer;

        this.RayCaster = new THREE.Raycaster(); // Used to check the user mouse position.

        this.DomElement = Utilities.PotreeViewer.renderer.domElement // The element from which we receive user events
        this.EventDispatcher = document.createElement('div'); // A blank DOM node used to dispatch events and assign event listeners.
    }

    addEventListener(name, callback) {
        this.EventDispatcher.addEventListener(name, callback);
    }



}