import { Interaction } from './Interaction.js';
import { Utilities } from '../utilities/Utilities.js';

import * as THREE from '../../libs/threejs/three.module.js';

export class SelectInteraction extends Interaction {
    
    constructor(config) {
        super(config);

        if (config) {
            if (config.source) this.ParentSource = config.source;
        }

        this.MovedMouse = false;
        this.SelectedObject = undefined;

    }

    async initializeEvents() {
        this.DomElement.addEventListener('pointerdown', (event) => { 
            this.MovedMouse = false;
        });

        this.DomElement.addEventListener('mousemove', (event) => {
            this.MovedMouse = true;
        });

        this.DomElement.addEventListener('pointerup', async (event) => {
            if (this.MovedMouse == false) {
                let intersect = this.getMouseIntersect(event);

                await this.ParentSource.handleSelectInteraction(intersect, this); // the source decides what is selected etc.

                if (intersect) {
                    const customEvent = new CustomEvent('selected', { detail: this.SelectedObject });
                    this.EventDispatcher.dispatchEvent(customEvent);
                }
            }
        });
    }

    getMouseIntersect(event) {
        if (this.ParentSource.Models) {
            let mouseCoords = new THREE.Vector2(
                ( event.layerX / this.DomElement.getBoundingClientRect().width) * 2 - 1, // x
                -( event.layerY / this.DomElement.getBoundingClientRect().height ) * 2 + 1 // y
            );

            this.RayCaster.setFromCamera(mouseCoords, this.PotreeViewer.scene.getActiveCamera());

            let intersects = this.RayCaster.intersectObjects( this.ParentSource.Models, true );
            intersects.sort((first, second) => (first.distance > second.distance) ? 1 : -1)

            return intersects[0];
        }
    }

}