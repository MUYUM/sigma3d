import { Utilities } from '../../utilities/Utilities.js';

import { Point } from '../../geometries/Point.js';
import { Line } from '../../geometries/Line.js';
import { Polygon } from '../../geometries/Polygon.js';

import { DrawHelperLineMaterial, DrawHelperMeshMaterial } from '../../utilities/Materials.js';

export class DrawHelper {
    constructor(config) {
        this.Vectors = [];
        
        this.Points = [];
        this.Line = undefined;
        this.Polygon = undefined;

        this.ThreeScene = Utilities.PotreeViewer.scene.scene;

        if (config) {
            if (config.type) this.Type = config.type;
            if (config.scene) this.ThreeScene = config.scene;
        }
    }

    addVector( vector ) {
        this.Vectors.push(vector);

        let point = new Point(vector,  {material: DrawHelperMeshMaterial});
        this.ThreeScene.add(point.model);
        this.Points.push(point);

        if (this.Type == "Line" && this.Vectors.length > 1) {
            console.log(this.Line)
            this.removeLine();

            let line = new Line(this.Vectors, {material: DrawHelperLineMaterial});
            this.ThreeScene.add(line.model);
            this.Line = line;
        }

        if (this.Type == "Polygon" && this.Vectors.length > 2) {
            this.removePolygon();

            let polygon = new Polygon( [ this.Vectors ], {material: DrawHelperMeshMaterial});

            if (!this.Polygon) {
                this.ThreeScene.add(polygon.model);
            }

            this.Polygon = polygon;
        }
    }

    clear() {
        this.removePolygon();
        this.removeLine();
        this.removePoints();

        this.Vectors = [];
    }

    removePolygon() {
        if (this.Polygon) {
            this.Polygon.model.geometry.dispose();
            this.ThreeScene.remove(this.Polygon.model);
            this.Polygon = undefined;
        }
    }

    removeLine() {
        if (this.Line) {
            this.Line.model.geometry.dispose();
            this.ThreeScene.remove(this.Line.model);
            this.Line = undefined;
        }
    }

    removePoints() {
        if (this.Points) {
            for (let point of this.Points) {
                this.ThreeScene.remove(point.model);
            }

            this.Points = [];
        }
    }

    convertPositionsToVectors( positions ) {
        let vectors = [];

        for (let [x,y,z] of positions) {
            vectors.push( new THREE.Vector3(x,y,z) );
        }
        
        return vectors;
    }
}