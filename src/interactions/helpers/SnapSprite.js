"use strict";

// This allows the user's mouse to "snap" to the vector.
class SnapSprite {
    constructor( [x,y,z], targetObject ) {
        this.targetObject = targetObject;
    }
}