"use strict";

import { Layer } from './Layer.js'

export class GeometryLayer extends Layer {
    constructor( config ) {
        super( config )
        this.Type = "GeometryLayer";
        this.Source = config.source;
    }

}