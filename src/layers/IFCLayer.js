"use strict"

import { Layer } from './Layer.js'

export class IFCLayer extends Layer {
    constructor( config ) {
        super( config )
        this.Type = "IFCLayer";
        this.Source = config.source;
    }
}