"use strict";

import { Layer } from './Layer.js'
import { Utilities } from '../utilities/Utilities.js'

export class Image360Layer extends Layer {
    constructor( config ) {
        super( config )
        this.Type = "Image360Layer";   
        this.Source = config.source;
    }
}