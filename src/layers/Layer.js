"use strict"

// This is the base class for layers and is only meant to be inherited from.

class Layer {
    constructor( config ) {
        this.Models = [];

        if (config.visible == true) {
            this.visible = config.visible;
            this.show();
        }
        else if( config.visible == false) {
            this.visible = config.visible;
            this.hide();
        }
        else 
            this.visible = true;

    }

    toggle() {
        if (this.visible)
            this.hide();
        else
            this.show();
    }

    hide() {
        this.visible = false;

        if (this.Models !== undefined && this.Models !== null) {
            for (let Model of this.Models) {
                Model.visible = false;
            }
        }
    }

    show() {
        this.visible = true;

        if (this.Models !== undefined && this.Models !== null) {
            for (let Model of this.Models) {
                Model.visible = true;
            }
        }
    }

}

export { Layer }