"use strict";

import { Layer } from './Layer.js'
import { Utilities } from '../utilities/Utilities.js'

class PointcloudLayer extends Layer {
    constructor( config ) {
        super( config )
        this.Type = "PointcloudLayer";
        this.Source = config.source;
    }
}

export { PointcloudLayer }