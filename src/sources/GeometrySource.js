import { Utilities } from "../utilities/Utilities.js";

import { Polygon } from "../geometries/Polygon.js";
import { Line } from "../geometries/Line.js";
import { Point } from "../geometries/Point.js";
import { HeightPoint } from "../geometries/HeightPoint.js";

import { 
    LineMaterial,
    PointMaterial,
    PolygonMaterial,
    HeightPointMaterial,

    MeshSelectionMaterial, LineSelectionMaterial 
} from "../utilities/Materials.js";

export class GeometrySource {
    
    constructor(config) {
        this.Geometries = [];
        this.Models = [];
        this.ThreeScene = Utilities.PotreeViewer.scene.scene;

        this.Type = "GeometrySource";

        if (config) {
            if (config.geojson) this.geoJSON = config.geojson;
            if (config.geometries) this.Geometries = config.geometries;
            if (config.scene) this.ThreeScene = scene;
        }
    }

    async load() {
        if (this.geoJSON) {
            let geometries = await this.geoJSONToGeometries(this.geoJSON);
            await this.loadGeometries(geometries)
        }

        if (this.Geometries) {
            await this.loadGeometries(this.Geometries);
        }
    }

    loadGeometries(input) {
        return new Promise( (resolve, reject) => {
            // generates models from already instantiated classes
            if (input instanceof Array) {
                for (let geometry of input) {
                    this.Models.push( geometry.model );
                }
            }

            resolve();
        });
    }

    geoJSONToGeometries(input) {
        return new Promise( (resolve, reject) => {
            let geometries = [];

            // generates models from GeoJSON
            if (typeof input === 'string') {
                let geoJSON = JSON.parse(input);

                for (let geometry of geoJSON) {
                    if (geometry.type == 'Point') { // Could be point or heightpoint? How do we solve this?
                        let point = new Point(geometry.coordinates);
                        geometries.push( point );
                    }

                    if (geometry.type == 'LineString') {
                        let line = new Line(geometry.coordinates);
                        geometries.push( line );
                    }

                    if (geometry.type == 'Polygon') {
                        let polygon = new Polygon( geometry.coordinates );
                        geometries.push(polygon)
                    }

                }
            }

            resolve(geometries);
        });
    }

    async add(input) {
        if (typeof input === 'string') {
            let geometries = await this.geoJSONToGeometries(this.geoJSON);
            await this.loadGeometries(geometries)
            
            for (let geometry of geometries) {
                this.ThreeScene.add(geometry.model);
            }
        }
        else if (input instanceof Array) {
            await this.loadGeometries(input);

            for (let geometry of input) {
                this.ThreeScene.add(geometry.model);
            }
        }
        else if (input !== null && input !== undefined) { // input is an object
            await this.loadGeometries([input]);
            this.ThreeScene.add(input.model);
        }
    }

    attachToScene() {
        for (let model of this.Models) {
            this.ThreeScene.add(model);
        }
    }

    // Interaction handling
    async handleSelectInteraction(intersect, interaction) {
        return new Promise( (resolve, reject) => {
            if (interaction.SelectedObject) {
                this.removeGeometryHighlight(interaction.SelectedObject);
            }

            if (intersect) {
                interaction.SelectedObject = intersect.object.userData
                this.highlightGeometry(interaction.SelectedObject);
            }

            resolve()
        });
    }

    // misc.
    highlightGeometry(geometry) {
        if (geometry.Type == 'Line') {
            geometry.model.material = LineSelectionMaterial;
        }
        else if(
            geometry.Type == 'Point' ||
            geometry.Type == 'Polygon' ||
            geometry.Type == 'HeightPoint' 
        ) {
            geometry.model.material = MeshSelectionMaterial;
        }
    }

    removeGeometryHighlight(geometry) {
        if (geometry.Type == 'Line') {
            geometry.model.material = LineMaterial;
        }
        if (geometry.Type == 'Point') {
            geometry.model.material = PointMaterial;
        }
        if (geometry.Type == 'Polygon') {
            geometry.model.material = PolygonMaterial;
        }
        if(geometry.Type == 'HeightPoint') {
            geometry.model.material = HeightPointMaterial;
        } 
    }


}