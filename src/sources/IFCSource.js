import { Utilities } from "../utilities/Utilities.js";
import * as THREE from '../../libs/threejs/three.module.js';

import { IFCSelectionMaterial } from "../utilities/Materials.js";

export class IFCSource {
    
    constructor(config) {
        this.Urls = [];
        this.Models = [];
        this.ThreeScene = Utilities.PotreeViewer.scene.scene;

        this.Type = "IFCSource";

        if (config) {
            if (config.urls) this.Urls = config.urls;
            if (config.scene) this.ThreeScene = scene;
        }
    }

    async load() {
        return new Promise( async(resolve, reject) => {
            if (this.Urls && this.Urls.length > 0) {
                for (let url of this.Urls) {
                    let model = await this.loadIFCFromUrl(url);
                    this.Models.push(model);
                }
            }

            resolve();
        });
    }

    async loadIFCFromUrl(url) {
        return new Promise( (resolve, reject) => {
            Utilities.IFCLoader.load( url, (model) => {
                this.offsetModel(model);
                resolve(model);
            });
        });
    }

    async add(input) { // input is a URL to the IFC model
        if (typeof input === 'string') {
            this.Urls.push(input);
            let model = await this.loadIFCFromUrl(input);
            this.Models.push(model);
            this.ThreeScene.add(model);
        }
    }

    attachToScene() {
        for (let model of this.Models) {
            this.ThreeScene.add(model);
        }
    }

    // Handle interactions
    async handleSelectInteraction(intersect, interaction) {
        return new Promise( (resolve, reject) => {
            if (intersect) {
                let scene = interaction.PotreeViewer.scene.scene;
                let id = Utilities.IFCLoader.ifcManager.getExpressId( intersect.object.geometry, intersect.faceIndex );
                let modelID = intersect.object.modelID;
                let subset = Utilities.IFCLoader.ifcManager.createSubset( { modelID, ids: [ id ], scene, removePrevious: true, material: IFCSelectionMaterial } );
                let props = Utilities.IFCLoader.ifcManager.getItemProperties( modelID, id, true );

                this.offsetModel(subset);

                interaction.SelectedObject = {
                    model:subset,
                    properties:props
                }
            }
            resolve();
        });
    }

    // misc
    offsetModel(model) {
        // Center the model and apply it's initial offset so we can rotate it properly
        let center = new THREE.Vector3();
        model.geometry.computeBoundingBox();
        model.geometry.boundingBox.getCenter(center);
        model.geometry.center();
        model.position.set(center.x, -center.z, center.y);

        // Rotate the model
        model.rotation.x = Math.PI / 2;
    }


}