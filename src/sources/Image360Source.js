import { Utilities } from "../utilities/Utilities.js";

export class Image360Source {
    
    constructor(config) {
        this.Urls = [];
        this.ImageSets = [];
        this.PotreeScene = Utilities.PotreeViewer.scene;
        this.Type = "Image360Source";

        if (config) {
            if (config.urls) this.Urls = config.urls;
            if (config.scene) this.PotreeScene = config.scene;
        }
    }

    async load() {
        return new Promise( async(resolve, reject) => {
            if (this.Urls && this.Urls.length > 0) {
                for (let url of this.Urls) {
                    let images = await this.loadImagesFromUrl(url);
                    this.ImageSets.push(images);   
                    console.log(images); 
                }
            }

            resolve();
        });
    }

    async loadImagesFromUrl(url) {
        return new Promise( (resolve, reject) => {
            Utilities.Potree.Images360Loader.load( url, Utilities.PotreeViewer ).then( images => {
                resolve(images);
            });
        })
    }

    async add(input) { // input is a URL to the Pointcloud
        if (typeof input === 'string') {
            this.Urls.push(input);
            let images = await this.loadImagesFromUrl(input);
            this.ImageSets.push(images);

            this.PotreeScene.add360Images(images);
            Utilities.PotreeViewer.fitToScreen();
        }
    }

    attachToScene() {
        for (let imageSet of this.ImageSets) {
            this.PotreeScene.add360Images(imageSet);
            Utilities.PotreeViewer.fitToScreen();
        }
    }


}