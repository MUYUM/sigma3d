import { Utilities } from "../utilities/Utilities.js";
import * as THREE from '../../libs/threejs/three.module.js';

export class PointcloudSource {
    
    constructor(config) {
        this.Urls = [];

        this.Pointclouds = [];
        this.Models = [];

        this.PotreeScene = Utilities.PotreeViewer.scene;

        this.Type = "PointcloudSource";

        if (config) {
            if (config.urls) this.Urls = config.urls;
            if (config.scene) this.PotreeScene = config.scene;
        }
    }

    async load() {
        return new Promise( async(resolve, reject) => {
            if (this.Urls && this.Urls.length > 0) {
                for (let url of this.Urls) {
                    let pointcloud = await this.loadPointcloudFromUrl(url);
                    this.Models.push(pointcloud);
                    this.Pointclouds.push(pointcloud);    
                }
            }

            resolve();
        });
    }

    async loadPointcloudFromUrl(url) {
        return new Promise( (resolve, reject) => {
            Utilities.Potree.loadPointCloud( url, "", (event) =>
            {
				let pointcloud = event.pointcloud;

				let material = pointcloud.material;
				material.size = 1;
				material.pointSizeType = Potree.PointSizeType.ADAPTIVE;
				material.shape = Potree.PointShape.SQUARE;

                resolve(pointcloud);
            });
        })
    }

    async add(input) { // input is a URL to the Pointcloud
        if (typeof input === 'string') {
            this.Urls.push(input);
            let pointcloud = await this.loadPointcloudFromUrl(input);

            this.Models.push(pointcloud);
            this.Pointclouds.push(pointcloud);

            this.PotreeScene.addPointCloud(pointcloud);
            Utilities.PotreeViewer.fitToScreen();
        }
    }

    attachToScene() {
        for (let pointcloud of this.Pointclouds) {
            this.PotreeScene.addPointCloud(pointcloud);
            Utilities.PotreeViewer.fitToScreen();
        }
    }


}