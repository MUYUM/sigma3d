"use strict";

import * as THREE from '../../libs/threejs/three.module.js'

// Points
export const PointGeometry = new THREE.SphereGeometry( 0.05, 16, 16 );

// HeightPoints
export const HeightPointGeometry = new THREE.SphereGeometry( 0.06, 16, 16 );

// Text, such as that used on HeightPoint labels
export const TextGeometry = new THREE.PlaneBufferGeometry(0.5,0.2);