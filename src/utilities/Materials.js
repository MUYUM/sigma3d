"use strict";

import * as THREE from '../../libs/threejs/three.module.js'

// -- Geometry Materials --
export const PointMaterial = new THREE.MeshBasicMaterial( {color: 'red'} );
export const HeightPointMaterial = new THREE.MeshBasicMaterial( {color: '#f0a37a'} );
export const LineMaterial = new THREE.LineBasicMaterial( {color: 'red'} );
export const PolygonMaterial = new THREE.MeshBasicMaterial( {color: 'purple', side: THREE.DoubleSide } );

// -- Interaction materials --
export const MeshSelectionMaterial = new THREE.MeshBasicMaterial( {color: 'lightgreen', depthTest: false, side: THREE.DoubleSide, transparent: true, opacity: 0.5 } );
export const LineSelectionMaterial = new THREE.LineBasicMaterial( {color: 'lightgreen', depthTest: false, transparent: true, opacity: 0.5} );

// Selected IFC Subset
export const IFCSelectionMaterial = new THREE.MeshPhongMaterial( { color: 'lightgreen', depthTest: false, transparent: true, opacity: 0.9 } );

// -- DrawHelper materials --
export const DrawHelperMeshMaterial = new THREE.MeshBasicMaterial( {color: 'grey', transparent: true, opacity: 0.3} );
export const DrawHelperLineMaterial = new THREE.LineBasicMaterial( {color: 'grey', depthTest: false, transparent: true, opacity: 0.5 });