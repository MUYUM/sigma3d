"use strict";

import {IFCLoader} from '../../libs/threejs/ifc/IFCLoader.js'

// Custom controls for Potree. The default for all maps unless otherwise specified.
import { noCloudControls } from '../../libs/potree/noCloudControls.js'

const Utilities = {

    IFCLoader: new IFCLoader(),
    
    // Potree is already globally declared
    Potree: Potree,
    PotreeViewer: new Potree.Viewer( document.getElementById('potree_render_area') ),
    noCloudControls: noCloudControls
}

// Utility configuration.
Utilities.IFCLoader.ifcManager.setWasmPath( './' );

// Configure the Potree viewer
Utilities.PotreeViewer.setEDLEnabled(true);
Utilities.PotreeViewer.setFOV(60);
Utilities.PotreeViewer.setPointBudget(500_000);
Utilities.PotreeViewer.loadSettingsFromURL();
Utilities.PotreeViewer.setBackground("white");
Utilities.PotreeViewer.noCloudControls = new Utilities.noCloudControls(Utilities.PotreeViewer);
Utilities.PotreeViewer.setControls(Utilities.PotreeViewer.noCloudControls);
Utilities.Controls = Utilities.PotreeViewer.noCloudControls;

Utilities.PotreeViewer.loadGUI(() => {
    Utilities.PotreeViewer.setLanguage('en');
    $("#menu_tools").next().show();
    $("#menu_scene").next().show();
    Utilities.PotreeViewer.toggleSidebar();
    Utilities.PotreeViewer.setLanguage('en');
});

let container = document.getElementById('potree_render_area').getBoundingClientRect();
Utilities.PotreeViewer.renderer.setSize( container.width, container.height );

// Potree is written in such a way that it often assumes "viewer" is a global variable.
// I'll try to fix it as I go along, but just in case:
window.viewer = Utilities.PotreeViewer;

export { Utilities }