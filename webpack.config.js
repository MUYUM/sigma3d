const path = require('path');

module.exports = {
  entry: './src/Sigma3D.js',
  output: {
    filename: 'Sigma3D.js',
    path: path.resolve(__dirname, 'build'),
  },
};